import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {InviteComponent} from './personne/invite/invite/invite.component';
import {EtudiantComponent} from './personne/etudiant/etudiant/etudiant.component';
import {AdminComponent} from './personne/admin/admin.component';
import {EmployeComponent} from './personne/employe/employe.component';
import {EtudiantListComponent} from './personne/etudiant/etudiant-list/etudiant-list.component';
import {EtudiantCreerComponent} from './personne/etudiant/etudiant-creer/etudiant-creer.component';
import {EtudiantDetailComponent} from './personne/etudiant/etudiant-detail/etudiant-detail.component';
import {EtudiantParIdComponent} from './personne/etudiant/etudiant-par-id/etudiant-par-id.component';
import {MatiereManageComponent} from './matiere/matiere-manage/matiere-manage.component';
import {MatiereListeComponent} from './matiere/matiere-liste/matiere-liste.component';
import {MatiereDetailComponent} from './matiere/matiere-detail/matiere-detail.component';
import {MatiereEditeComponent} from './matiere/matiere-edite/matiere-edite.component';
import {MatiereCreateComponent} from './matiere/matiere-create/matiere-create.component';
import {AuthGuard} from './shared/guard/matiere/auth-guard.';
import {CanDeactivateGuard} from './shared/guard/matiere/can-deactivate.guard';


const routes: Routes = [

  {path: '', redirectTo: '/accueil', pathMatch: 'full'},
  {path: 'accueil', component: AccueilComponent},
  {path: 'invite', component: InviteComponent},
  {
    path: 'etudiant',
    component: EtudiantComponent,
    children: [
      {
        path: '',
        component: EtudiantListComponent

      },
      {
        path: 'creerEtudiant',
        component: EtudiantCreerComponent

      },
      {
        path: 'id/detail',
        component: EtudiantDetailComponent

      },
      {
        path: 'id',
        component: EtudiantParIdComponent

      },
    ]
  },
  {
    path: 'administration',
    component: AdminComponent
  },
  {
    path: 'employe',
    component: EmployeComponent
  },
  {
    path: 'matiere',
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: MatiereManageComponent

      },
      {
        path: 'liste',
        canActivateChild: [AuthGuard],
        component: MatiereListeComponent,
        children: [
          {
            path: ':id/detail',
            component: MatiereDetailComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: ':id/edite',
            component: MatiereEditeComponent
          },
          {
            path: 'creer', component: MatiereCreateComponent
          }

        ]

      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
