import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';


import {
  AutoCompleteModule,
  ButtonModule,
  CheckboxModule,
  ConfirmationService,
  ConfirmDialogModule,
  ContextMenuModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  GMapModule, GrowlModule, InputMaskModule, OrderListModule, PanelMenuModule,
  SharedModule
} from 'primeng/primeng';
import {EtudiantComponent} from './personne/etudiant/etudiant/etudiant.component';
import {InviteComponent} from './personne/invite/invite/invite.component';

import {NavbarComponent} from './shared/cadre/navbar/navbar.component';
import {MenubarComponent} from './shared/cadre/menubar/menubar.component';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {FootbarComponent} from './shared/cadre/footbar/footbar.component';
import {AdminComponent} from './personne/admin/admin.component';
import {EmployeComponent} from './personne/employe/employe.component';

import {EtudiantDetailComponent} from './personne/etudiant/etudiant-detail/etudiant-detail.component';
import {EtudiantListComponent} from './personne/etudiant/etudiant-list/etudiant-list.component';
import {EtudiantEditComponent} from './personne/etudiant/etudiant-edit/etudiant-edit.component';
import {EtudiantCreerComponent} from './personne/etudiant/etudiant-creer/etudiant-creer.component';
import {EtudiantParIdComponent} from './personne/etudiant/etudiant-par-id/etudiant-par-id.component';
import {InviteService} from './shared/service/personne/invite/invite.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {AlertModule} from 'ngx-bootstrap';


import {MatiereListeComponent} from './matiere/matiere-liste/matiere-liste.component';
import {MatiereCreateComponent} from './matiere/matiere-create/matiere-create.component';
import {MatiereEditeComponent} from './matiere/matiere-edite/matiere-edite.component';
import {MatiereManageComponent} from './matiere/matiere-manage/matiere-manage.component';
import {MatiereDetailComponent} from './matiere/matiere-detail/matiere-detail.component';
import {MatiereService} from './shared/service/matiere/matiere.service';
import {AuthGuard} from './shared/guard/matiere/auth-guard.';
import {CanDeactivateGuard} from './shared/guard/matiere/can-deactivate.guard';

import {EnseignantModule} from './personne/enseignant/enseignant.module';
import {MatIconModule} from '@angular/material';




@NgModule({
  declarations: [
    AppComponent,
    EtudiantComponent,
    InviteComponent,
    NavbarComponent,
    MenubarComponent,
    AccueilComponent,
    FootbarComponent,
    AdminComponent,
    EmployeComponent,
    EtudiantDetailComponent,
    EtudiantListComponent,
    EtudiantEditComponent,
    EtudiantCreerComponent,
    EtudiantParIdComponent,
    MatiereCreateComponent,
    MatiereEditeComponent,
    MatiereManageComponent,
    MatiereDetailComponent,
    MatiereListeComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule.forRoot(),
    ButtonModule,
    AppRoutingModule,
    DataTableModule,
    DialogModule,
    SharedModule,
    BrowserAnimationsModule,
    DropdownModule,
    GMapModule,
    GrowlModule,
    CheckboxModule,
    ContextMenuModule,
    ConfirmDialogModule,
    InputMaskModule,
    PanelMenuModule,
    OrderListModule,
    AutoCompleteModule,
    EnseignantModule,
    MatIconModule



  ],
  providers: [InviteService, ConfirmationService, MatiereService, AuthGuard, CanDeactivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
