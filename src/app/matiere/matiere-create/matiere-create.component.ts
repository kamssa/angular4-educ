import {Component, OnInit} from '@angular/core';
import {Matiere} from '../../shared/modele/matiere/matiere';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-matiere-create',
  templateUrl: './matiere-create.component.html',
  styleUrls: ['./matiere-create.component.scss']
})
export class MatiereCreateComponent implements OnInit {

  matieres: Matiere[] = [];
  newMatiere: boolean;
  successMesage: string;
  statusSucces: number;
  errorMessage: string;
  titre = 'Creer une matiere'
  matiere = new Matiere(null, null, '', '');

  constructor(private  matiereService: MatiereService, private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.matiere);
    this.matiereService.ajoutMatiere(this.matiere)
      .subscribe(res => {
          this.matiere = res.body;
          this.successMesage = res.messages.toString();
          this.statusSucces = res.statut;

        }
      );
 this.router.navigate(['/matiere/liste']);
  }
  annuler(){
    this.router.navigate(['/matiere/liste']);
  }
}



