import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatiereEditeComponent } from './matiere-edite.component';

describe('MatiereEditeComponent', () => {
  let component: MatiereEditeComponent;
  let fixture: ComponentFixture<MatiereEditeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatiereEditeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatiereEditeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
