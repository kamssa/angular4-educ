import {Component, OnInit} from '@angular/core';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {Matiere} from '../../shared/modele/matiere/matiere';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

@Component({
  selector: 'app-matiere-edite',
  templateUrl: './matiere-edite.component.html',
  styleUrls: ['./matiere-edite.component.scss']
})
export class MatiereEditeComponent implements OnInit {
  matiere: Matiere;
  titre = 'Modification du detail'


  constructor(private matiereService: MatiereService, private  route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.route.paramMap.switchMap((params: ParamMap) =>
      this.matiereService.getMatiereById(+params.get('id')))
      .subscribe(res => {
        this.matiere = res.body;
      });

  }

  onSubmit() {
    console.log('Modification');
    this.matiereService.modifierUneMatiere(this.matiere)
      .subscribe(res =>
        console.log('la matiere modifiee: '));
    this.router.navigate(['/matiere/liste']);
  }

  annuler() {
    this.router.navigate(['/matiere/liste']);
  }
}
