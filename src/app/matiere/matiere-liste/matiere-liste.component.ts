import {Component, OnInit} from '@angular/core';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {Matiere} from '../../shared/modele/matiere/matiere';
import {Route, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/of';


@Component({
  selector: 'app-matiere-liste',
  templateUrl: './matiere-liste.component.html',
  styleUrls: ['./matiere-liste.component.scss']
})
export class MatiereListeComponent implements OnInit {
  title = 'LES MATIERES';
  omatieres: Observable<Matiere[]>
  matieres: Matiere[] = [];
  _statut: number;
  errorMessage: string;
  errorMessageStatus: string;
  selectedMatiere: Matiere;
  messageSucces: string;
  searchItemeSource = new Subject<string>();


  constructor(private matiereService: MatiereService, private route: Router) {
  }

  ngOnInit() {

    this.omatieres = this.searchItemeSource
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(mc => mc ? this.matiereService.rechercheMatiereParMc(mc)
        : this.matiereService.rechercheMatiereParMc('personne non trouve'));

    this.toutesLesMatieres();

    // renvoie la matiere creer
    this.matiereService.matiereCreer$.subscribe(res => {
      this.closeMessage();
      this.matieres.push(res.body);
      this.messageSucces = res.messages.toString();

    });
    this.matiereService.matiereSupprimer$.subscribe(
      res => {
        this.closeMessage();
        let index: number;
        index = this.findSelectedMatiereIndex();
        this.matieres = this.matieres.filter((val, i) => i !== index);
        this.messageSucces = res.messages.toString();
      }
    );
    this.matiereService.matiereModifier$.subscribe(
      res => {
        this.closeMessage();
        this.matieres[this.findSelectedMatiereIndex()] = res.body;
        this.messageSucces = res.messages.toString();

      }
    );
    this.matiereService.matiereFiltre$
      .subscribe(lib => {
          this.search(lib);
        }
      );
  }

  findSelectedMatiereIndex(): number {
    return this.matieres.indexOf(this.selectedMatiere);
  }

  toutesLesMatieres() {
    this.matiereService.getAllMatieres()
      .subscribe(data => {
          this.matieres = data.body;
          this._statut = data.statut;
        },
        error => {
          if (error.status = 0) {
            this.errorMessageStatus = 'Problème de connexion!';
          } else {
            this.errorMessage = error;
          }
        })
    ;
  }

  onSelect(matiere: Matiere): void {
    this.selectedMatiere = matiere;
    this.route.navigate(['/matiere/liste', matiere.id, 'detail']);

  }

  closeMessage() {
    setTimeout(() => {
      this.messageSucces = '';
    }, 5000);
  }

  search(mc: string) {
    this.searchItemeSource.next(mc);
  }
}
