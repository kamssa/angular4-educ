import {Component, OnInit} from '@angular/core';
import {Enseignant} from '../../../shared/modele/personne/enseignant';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {Subject} from 'rxjs/Subject';
import {Resultat} from '../../../shared/modele/resultat';

@Component({
  selector: 'app-enseignant-details',
  templateUrl: './enseignant-details.component.html',
  styleUrls: ['./enseignant-details.component.scss']
})
export class EnseignantDetailsComponent implements OnInit {
  enseignant: Enseignant;
  pathNullImage = './assets/images/image3.jpg';
  constructor(private ensService: EnseignantService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.route.paramMap.switchMap((params: ParamMap) =>
      this.ensService.getEnseignantById(+params.get('id')))
      .subscribe(res => {
        this.enseignant = res.body;
      });

  }

  editerEnseignant() {
    this.router.navigate(['/enseignant/liste', this.enseignant.id, 'editer']);
  }

  ajouterPhoto() {
    this.router.navigate(['/enseignant/liste', this.enseignant.id, 'photo']);
  }

  supprimerEnseignant() {

  }

}
