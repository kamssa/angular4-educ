import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnseignantEditerComponent } from './enseignant-editer.component';

describe('EnseignantEditerComponent', () => {
  let component: EnseignantEditerComponent;
  let fixture: ComponentFixture<EnseignantEditerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnseignantEditerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnseignantEditerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
