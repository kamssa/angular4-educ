import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnseignantListeComponent } from './enseignant-liste.component';

describe('EnseignantListeComponent', () => {
  let component: EnseignantListeComponent;
  let fixture: ComponentFixture<EnseignantListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnseignantListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnseignantListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
