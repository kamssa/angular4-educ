import {Component, OnInit} from '@angular/core';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {Enseignant} from '../../../shared/modele/personne/enseignant';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


@Component({
  selector: 'app-enseignant-liste',
  templateUrl: './enseignant-liste.component.html',
  styleUrls: ['./enseignant-liste.component.scss']
})
export class EnseignantListeComponent implements OnInit {
  title = 'la liste des enseignants';
  enseignants: Enseignant[] = [];
  enseignant: Enseignant;
  selectedEnseignant: Enseignant;
  messageSucces: string;
  statut: number;
  oenseignants: Observable<Enseignant[]>;
  searchEnseignantSource = new BehaviorSubject<string>('');
  pathNullImage = './assets/images/image3.jpg';

  constructor(private ensSevice: EnseignantService, private router: Router) {
  }

  ngOnInit() {
    this.ensSevice.enseignnantFiltre$
      .subscribe(text => {
        this.search(text);
        console.log('text recuperer', text);
      });
    this.oenseignants = this.searchEnseignantSource
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(mc => mc ? this.ensSevice.rechercheEnseignantParMc(mc)
        : this.ensSevice.rechercheEnseignantParMc(' '));

    this.toutsLesEnseignants();
    this.ensSevice.enseignantCreer$.subscribe(res => {
        this.enseignants.push(res.body);
        this.messageSucces = res.messages.toString();

      }
    );
    this.ensSevice.enseignantModif$.subscribe(res => {
        this.enseignants[this.findSelectedEnseignantIndex()] = res.body;
        this.messageSucces = res.messages.toString();
      }
    );

  }

  toutsLesEnseignants() {
    this.ensSevice.getAllEnseignants()
      .subscribe(data => {
        this.enseignants = data.body;
        this.statut = data.statut;

      });

  }

  findSelectedEnseignantIndex(): number {
    return this.enseignants.indexOf(this.selectedEnseignant);
  }


  onSelect(enseignant: Enseignant) {
    this.selectedEnseignant = enseignant;
    this.router.navigate(['enseignant/liste', enseignant.id]);
  }

  search(mc: string) {
    this.searchEnseignantSource.next(mc);
  }
}
