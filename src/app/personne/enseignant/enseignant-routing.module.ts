import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EnseignantManageComponent} from './enseignant-manage/enseignant-manage.component';
import {EnseignantListeComponent} from './enseignant-liste/enseignant-liste.component';
import {EnseignantDetailsComponent} from './enseignant-details/enseignant-details.component';
import {EnseignantEditerComponent} from './enseignant-editer/enseignant-editer.component';
import {EnseignantComponent} from './enseignant/enseignant.component';
import {EnseignantDebutComponent} from "./enseignant-debut/enseignant-debut.component";
import {EnseignantPhotoComponent} from "./enseignant-photo/enseignant-photo.component";


const enseignantRoutes: Routes = [
  {
    path: 'enseignant', component: EnseignantComponent,
    children: [

      {
        path: '', component: EnseignantManageComponent
      },
      {
        path: 'liste',
        component: EnseignantListeComponent,
        children: [
          {path: '', component: EnseignantDebutComponent},
          {
            path: 'creer', component: EnseignantEditerComponent
          },
          {path: 'creerPhoto', component: EnseignantPhotoComponent},

          {
            path: ':id', component: EnseignantDetailsComponent
          },
          {
            path: ':id/editer', component: EnseignantEditerComponent
          },
          {path: ':id/photo', component: EnseignantPhotoComponent}

        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(enseignantRoutes)
  ],
  exports: [RouterModule]
})
export class EnseignantRoutingModule {
}
