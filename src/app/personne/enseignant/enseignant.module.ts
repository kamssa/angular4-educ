import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import 'hammerjs';
import {
  MatInputModule,
  MatButtonModule, MatCardModule, MatSidenavModule, MatToolbarModule, MatIconModule, MatTooltipModule,
  MatProgressBarModule, MatSelectModule, MatExpansionModule, MatChipsModule
} from '@angular/material';

import {EnseignantListeComponent} from './enseignant-liste/enseignant-liste.component';
import {EnseignantManageComponent} from './enseignant-manage/enseignant-manage.component';
import {EnseignantEditerComponent} from './enseignant-editer/enseignant-editer.component';
import {EnseignantDetailsComponent} from './enseignant-details/enseignant-details.component';
import {EnseignantComponent} from './enseignant/enseignant.component';
import {EnseignantRoutingModule} from './enseignant-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EnseignantService} from '../../shared/service/personne/enseignant/enseignant.service';
import { EnseignantDebutComponent } from './enseignant-debut/enseignant-debut.component';
import { EnseignantPhotoComponent } from './enseignant-photo/enseignant-photo.component';



@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatSidenavModule,
    MatToolbarModule,
    EnseignantRoutingModule,
    MatIconModule,
    MatProgressBarModule,
    MatTooltipModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatExpansionModule,
    MatChipsModule



  ],
  declarations: [
    EnseignantListeComponent,
    EnseignantManageComponent,
    EnseignantEditerComponent,
    EnseignantDetailsComponent,

    EnseignantComponent,

    EnseignantDebutComponent,

    EnseignantPhotoComponent
  ],
  providers: [EnseignantService]
})
export class EnseignantModule {
}
