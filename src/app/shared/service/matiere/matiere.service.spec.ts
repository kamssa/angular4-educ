import { TestBed, inject } from '@angular/core/testing';

import { MatiereService } from './matiere.service';

describe('MatiereService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MatiereService]
    });
  });

  it('should ...', inject([MatiereService], (service: MatiereService) => {
    expect(service).toBeTruthy();
  }));
});
