import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Matiere} from '../../modele/matiere/matiere';
import {Observable} from 'rxjs/Observable';
import {Resultat} from '../../modele/resultat';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class MatiereService {
  // observable source
  private urlMatiere = 'http://localhost:8080/matieres';
  private urlRecherche = 'http://localhost:8080/rechemc/?mc=';
  private matiereCreerSource = new Subject<Resultat<Matiere>>();
  private matiereSupprimerSource = new Subject<Resultat<boolean>>();

  private matiereModifierSource = new Subject<Resultat<Matiere>>();

  private matiereFiltreSource = new Subject<string>();

  // comme observateur
  matiereCreer$ = this.matiereCreerSource.asObservable();
  matiereSupprimer$ = this.matiereSupprimerSource.asObservable();
  matiereModifier$ = this.matiereModifierSource.asObservable();
  matiereFiltre$ = this.matiereFiltreSource.asObservable();

  constructor(private http: Http) {
  }

  getAllMatieres(): Observable<Resultat<Matiere[]>> {
    return this.http.get(this.urlMatiere)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }

  ajoutMatiere(matiere: Matiere): Observable<Resultat<Matiere>> {
    return this.http.post(this.urlMatiere, matiere)
      .map(res => res.json())
      .do(data => {
        this.matiereCreer(data);
        this.matiereFiltre(data.body.libelle);
      }).catch(this._errorHandler);

  }

  getMatiereById(id: number): Observable<Resultat<Matiere>> {
    return this.http.get(`${this.urlMatiere}/${id}`)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }

  rechercheMatiereParMc(mc: string): Observable<Matiere[]> {
    return this.http.get(`${this.urlRecherche}${mc}`)
      .map(res => res.json().body)
      .do(data => console.log('recherche', data))
      .catch(this._errorHandler);
  }

  modifierUneMatiere(modifMatiere: Matiere): Observable<Resultat<Matiere>> {
    return this.http.put(this.urlMatiere, modifMatiere)
      .map(res => res.json())
      .do(data => {
        this.matiereModifier(data);
        this.matiereFiltre(data.body.libelle);
        console.log('retour de modifier une matiere');
        console.log(data);
      }).catch(this._errorHandler);

  }

  supprimerUneMatiere(id: number): Observable<Resultat<boolean>> {
    return this.http.delete(`${this.urlMatiere}/${id}`)
      .map(res => res.json())
      .do(data => {
        this.matiereSupprimer(data);
        this.matiereFiltre('')
        console.log('supprimer une matiere');
        console.log(data);
      }).catch(this._errorHandler);
  }

  matiereCreer(res: Resultat<Matiere>) {
    this.matiereCreerSource.next(res);
  }

// lorque l'utilisateur est supprime, on ajoute l'info a notre stream
  matiereSupprimer(r: Resultat<boolean>) {
    this.matiereSupprimerSource.next(r);
  }

  matiereModifier(res: Resultat<Matiere>) {
    this.matiereModifierSource.next(res);
  }

  matiereFiltre(libelle: string) {
    this.matiereFiltreSource.next(libelle);
  }

  // recuper les errurs
  _errorHandler(err) {
    let erreMessage: string;
    if (err instanceof Response) {
      const body = err.json() || '';
      const erreur = body.error || JSON.stringify(body);
      erreMessage = `${err.status} - ${err.statusText} || "}" ${erreur}`;

    } else {
      erreMessage = err.message ? err.message : err.toString();
    }

    return Observable.throw(erreMessage);
  }
}
