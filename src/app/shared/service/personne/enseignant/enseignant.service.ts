import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Resultat} from '../../../modele/resultat';
import {Enseignant} from '../../../modele/personne/enseignant';
import {Subject} from 'rxjs/Subject';


@Injectable()
export class EnseignantService {
  private urlEseignant = 'http://localhost:8080/typePersonnes/EN';
  private urlPersonne = 'http://localhost:8080/personnes';
  private urlPhoto = 'http://localhost:8080/photo';
  private urlPhoto1 = 'http://localhost:8080/getPhoto';
  private urlRecherche = 'http://localhost:8080/recheEnsmc/?mc=';
  // observables sources
  private enseignantCreerSource = new Subject<Resultat<Enseignant>>();
  private enseignantModifSource = new Subject<Resultat<Enseignant>>();
  private enseignantFiltreSource = new Subject<string>();


// observables streams
  enseignantCreer$ = this.enseignantCreerSource.asObservable();
  enseignantModif$ = this.enseignantModifSource.asObservable();
  enseignnantFiltre$ = this.enseignantFiltreSource.asObservable();

  constructor(private  http: Http) {
  }

  getAllEnseignants(): Observable<Resultat<Enseignant[]>> {
    return this.http.get(this.urlEseignant)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }

  ajoutEnseignant(ens: Enseignant): Observable<Resultat<Enseignant>> {
    console.log('methode du service qui ajoute un enseignant', ens);
    return this.http.post(this.urlPersonne, ens)
      .map(res => res.json())
      .do(data => {
        this.enseignantCreer(data);
        this.filtreEnseignant(data.body.nomComplet);
      })
      .catch(this._errorHandler);

  }

  modifierEnseignant(ensModif: Enseignant): Observable<Resultat<Enseignant>> {
    return this.http.put(this.urlPersonne, ensModif)
      .map(res => res.json())
      .do(data => {
        this.enseignantModif(data);
        this.filtreEnseignant(data.body.nomComplet);
        console.log(data);
      })
      .catch(this._errorHandler);

  }

  // supprimer un enseignant
  supprimerEnseignant(id: number): Observable<Resultat<boolean>> {
    return this.http.delete(`${this.urlPersonne}/${id}`)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }

  getEnseignantById(id: number): Observable<Resultat<Enseignant>> {
    return this.http.get(`${this.urlPersonne}/${id}`)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }

  enregistrerPhoto(imageFile: File, cni: string) {

    const formData: FormData = new FormData();
    formData.append('image_photo', imageFile, cni);
    return this.http.post(this.urlPhoto, formData)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);

  }

  /*getPhoto(cni: string): Observable<Resultat<File>> {
    return this.http.get(`${this.urlPhoto1}/${cni}`)
     .map(res => res)
      .do(data => console.log(data))
      .catch(this._errorHandler);

  }*/
  rechercheEnseignantParMc(mc: string): Observable<Enseignant[]> {
    return this.http.get(`${this.urlRecherche}${mc}`)
      .map(res => res.json().body)
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }

  enseignantCreer(res: Resultat<Enseignant>) {
    console.log('enseignant a ete  creer correctement essaie source');
    this.enseignantCreerSource.next(res);
  }

  enseignantModif(res: Resultat<Enseignant>) {
    this.enseignantModifSource.next(res);
  }

  filtreEnseignant(text: string) {
    this.enseignantFiltreSource.next(text);
  }

  ///////////////////////////////////////////
  // recuper les errurs
  _errorHandler(err) {
    let erreMessage: string;
    if (err instanceof Response) {
      const body = err.json() || '';
      const erreur = body.error || JSON.stringify(body);
      erreMessage = `${err.status} - ${err.statusText} ||  ${erreur}`;

    } else {
      erreMessage = err.message ? err.message : err.toString();
      console.log(erreMessage);
    }

    return Observable.throw(erreMessage);

  }
}
